package HomeWorks.lesson5;
import java.util.*;

public class Task7_8 {

    public static void main (String[]args)
    {

        Scanner sa = new Scanner (System.in);

        int[][] arr = new int[3][3];
        System.out.println ("9 reqem daxil edin:");


        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                arr[i][j] = sa.nextInt ();
            }
        }
        System.out.println ("sizin array:");
        System.out.print ("[");
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                System.out.print (arr[i][j]);
                if (j != arr[0].length - 1)
                {
                    System.out.print (", ");
                }
            }
            if (i != arr.length - 1)
            {
                System.out.println ();
            }
        }

        System.out.println ("]");

        String leftDiagonalElements = "";
        String rightDiagonalElements = "";

        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                if(i==j){
                    leftDiagonalElements= leftDiagonalElements + arr[i][j] + ", ";
                }
                if(i+j==arr[0].length-1){
                    rightDiagonalElements=rightDiagonalElements + arr[i][j] + ", ";
                }

            }
        }

        System.out.println("Sol diagonal elementleri: "+ leftDiagonalElements);
        System.out.println("Sag diagonal elementleri: "+ rightDiagonalElements);




    }
}
