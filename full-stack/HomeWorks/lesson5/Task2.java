package HomeWorks.lesson5;

import java.util.Scanner;

public class Task2
{
    public static void main (String[]args)
    {
        System.out.println ("9 reqem daxil edin");
        int[][] arr = new int[3][3];
        Scanner sa = new Scanner (System.in);

        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                arr[i][j] = sa.nextInt ();
            }
        }

        System.out.println("Array: ");
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                System.out.print (arr[i][j]);
                if (j != arr[0].length)
                {
                    System.out.print (", ");
                }
            }
            System.out.println ();
        }



        int leftDiagonalSum = 0;
        int rightDiagonalSum = 0;
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[0].length; j++)
            {
                if (i == j)
                {
                    leftDiagonalSum += arr[i][j];
                }
                else if (i + j == arr[0].length - 1)
                {
                    rightDiagonalSum += arr[i][j];
                }
            }
        }

        System.out.println ("sol diaqonalin cemi: " + leftDiagonalSum);
        System.out.println ("sag diaqonalin cemi: " + rightDiagonalSum);

    }
}

