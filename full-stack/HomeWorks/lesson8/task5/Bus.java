package HomeWorks.lesson8.task5;

public class Bus extends Transportation {
    public int wheelCount;

    public Bus(int wheelCount, int peopleCount) {
        super(peopleCount);
        this.wheelCount = wheelCount;
    }
}
