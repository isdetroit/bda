package HomeWorks.lesson12.tasks.enums;

public enum Currency {
    AZN("AZN"),
    USD("USD"),
    EU("EU");

    private String val;

    Currency(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
