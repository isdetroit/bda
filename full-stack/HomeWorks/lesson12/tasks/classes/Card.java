package HomeWorks.lesson12.tasks.classes;

import HomeWorks.lesson12.tasks.enums.Currency;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Card {

    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    public int id;
    public String cardNumber;
    public String currency;
    public String password;

    public LocalDateTime createDate;
    public LocalDateTime updateDate;
    public int balance; // bunu Double a kecirt

    public int fieldsCount = 4;

    public Card(int id, String cardNumber, String currency, String password, int balance, LocalDateTime createDate,LocalDateTime updateDate) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.currency = currency;
        this.password = password;
        this.balance = balance;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }



    @Override
    public String toString() {
      if(updateDate!=null){
          return "Card{" +
                  "id=" + id +
                  ", cardNumber='" + cardNumber + '\'' +
                  ", currency='" + currency + '\'' +
                  ", password='" + password + '\'' +
                  ", createDate=" + createDate.format(format) +
                  ", updateDate=" + updateDate.format(format) +
                  ", balance=" + balance +
                  ", fieldsCount=" + fieldsCount +
                  '}';
      }else{
          return "Card{" +
                  "id=" + id +
                  ", cardNumber='" + cardNumber + '\'' +
                  ", currency='" + currency + '\'' +
                  ", password='" + password + '\'' +
                  ", createDate=" + createDate.format(format) +
                  ", updateDate=" + updateDate +
                  ", balance=" + balance +
                  ", fieldsCount=" + fieldsCount +
                  '}';
      }
    }
}
