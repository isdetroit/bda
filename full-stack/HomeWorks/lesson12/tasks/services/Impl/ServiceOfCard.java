package HomeWorks.lesson12.tasks.services.Impl;

import HomeWorks.lesson12.tasks.classes.Card;
import HomeWorks.lesson12.tasks.enums.Currency;
import HomeWorks.lesson12.tasks.services.InterfaceService.CardServiceInterface;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;


public class ServiceOfCard implements CardServiceInterface {

    ArrayList<Card> cards = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);
    int id = 0;
    int enterId = -1;
    boolean isEnter = false;


    public void showMenu(){
        System.out.println("1- create");
        System.out.println("2- enter");


        System.out.println("emr secin");
        String emr = scanner.next();
        switch (emr) {
            case "1" -> create();
            case "2" -> enter();
        }

    }

    public void enter(){
        System.out.println("kart nomresini daxil edin:");
        String enterCard = scanner.next();
        for (Card card : cards) {
            if (card.cardNumber.equals(enterCard)) {
                enterId = card.id;
                isEnter = true;
            }
        }

        if(isEnter){
            System.out.println(enterId + " - id`si ile daxil olmusuz");
        }else{
            System.out.println("yanlis kart nomresi");
        }


        System.out.println("1- ShowAll");
        System.out.println("2- logout");
        System.out.println("3- azn -> usd");
        System.out.println("4- usd -> azn");
        System.out.println("5- delete");
        System.out.println("6- getBalance");
        System.out.println("7- update password");

        System.out.println("emr secin");
        String emr = scanner.next();
        switch (emr) {
            case "1" -> showAll();
            case "2" -> logout();
            case "3" -> aznToUSD();
            case "4" -> USDtoAzn();
            case "5" -> delete();
            case "6" -> getBalance();
            case "7" -> updatePassword();
        }

    }

    public void logout(){
         enterId = -1;
         isEnter = false;
         showMenu();
    }

    @Override
    public void create() {

        System.out.println("kard nomresi daxil:");
        String cardNumber = scanner.next();
        System.out.println("Password daxil edin:");
        String password = scanner.next();
        System.out.println("valyuta secin:");
        System.out.println("AZN - USD - EU");
        String currency = scanner.next();
        System.out.println("balans daxil edin:");
        int balance = scanner.nextInt();
        LocalDateTime createdate = LocalDateTime.now();
        LocalDateTime updateDate = null;

        Card card = new Card(id,cardNumber,currency,password,balance, createdate, updateDate);
        cards.add(card);
        id+=1;

        showMenu();

    }

    @Override
    public void showAll() {
        System.out.println(cards.get(enterId).toString());
    }

    @Override
    public void aznToUSD() {
    if(cards.get(enterId).currency.equals(Currency.AZN.name())){
        System.out.println("sizin hesab: " + cards.get(enterId).balance);
        System.out.println("dollarla: " + (cards.get(enterId).balance/1.7));
    }else{
        System.out.println("sizin hesab AZN - le deyil");
    }
    }

    @Override
    public void USDtoAzn() {
        if(cards.get(enterId).currency.equals(Currency.USD.name())){
            System.out.println("sizin hesab: " + cards.get(enterId).balance);
            System.out.println("azn-le: " + (cards.get(enterId).balance*1.7));
//        cards.get(enterId).balance = cards.get(enterId).balance*1.7;
        }else{
            System.out.println("sizin hesab USD - le deyil");
        }
    }

    @Override
    public void delete() {
    cards.remove(enterId);
    logout();
    }

    @Override
    public void getBalance() {
        System.out.println("sizin balance: " + cards.get(enterId).balance);
    }

    @Override
    public void updatePassword() {
        System.out.println("kohne password-u yazin");
        String oldPass = scanner.next();
        if(cards.get(enterId).password.equals(oldPass)){
            System.out.println("yeni password-u yazin");
            String newPass = scanner.next();
            System.out.println("yeni password-u tekrarlayin");
            String newPass2 = scanner.next();
            if(newPass.equals(newPass2)){
                System.out.println("ugurla deyisdirildi");
                cards.get(enterId).password = newPass;
                cards.get(enterId).updateDate = LocalDateTime.now();
                showAll();
            }else{
                System.out.println("yanlis tekrarladiniz");
                enter();
            }
        }else{
            System.out.println("parol yanlisdir, yeniden yazin");
            updatePassword();
        }
    }

}
