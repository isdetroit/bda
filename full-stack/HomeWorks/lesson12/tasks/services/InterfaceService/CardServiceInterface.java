package HomeWorks.lesson12.tasks.services.InterfaceService;

public interface CardServiceInterface {
    void create();
    void showAll();
    void aznToUSD();
    void USDtoAzn();
    void delete();
    void getBalance();
    void updatePassword();
}
