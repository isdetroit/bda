package HomeWorks.lesson2;

import java.util.Scanner;

public class Task2_11_12_13{

    public static void main(String []args) {
        // task2();
        // task11();
        // task12();
        // task13();
        task17();
    }
    // Ikinci task
    public static void task2() {
        System.out.println("a deyisenini qeyd edin");
        Scanner sc = new Scanner(System.in);
        int  a = sc.nextInt();
        System.out.println("b deyisenini qeyd edin");
        Scanner sb = new Scanner(System.in);
        int  b = sb.nextInt();
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("a: " + a);
        System.out.println("b: " + b);

    }

    // 11. Task
    public static void task11() {
        Scanner sa = new Scanner(System.in);
        int a = sa.nextInt();
        String b = String.valueOf(a);
        int c = b.length() - 5;
        int ten = 1;
        for(int i=0; i<c; i++) {
            ten*=10;
        }
        a = a/ten;
        System.out.println(a);
    }
    //TASK 12
    public static void task12() {
        System.out.println("a ededini daxil edin:");
        Scanner sa = new Scanner(System.in);
        int a = sa.nextInt();

        System.out.println("b ededini daxil edin:");
        Scanner sb = new Scanner(System.in);
        int b = sb.nextInt();

        int  c= (a%10) + (b%10);
        System.out.println("netice: " + c);

    }
    //TASK 13
    public static void task13() {
        System.out.println("a ededini daxil edin:");
        Scanner sa = new Scanner(System.in);
        int a = sa.nextInt();

        System.out.println("b ededini daxil edin:");
        Scanner sb = new Scanner(System.in);
        int b = sb.nextInt();

        int  c= a;
        a=b;
        b=c;
        System.out.println("a: " + a);
        System.out.println("b: " + b);

    }

    //TASK 17
    public static void task17() {
        System.out.println("birinci sozu daxil edin:");
        Scanner sa = new Scanner(System.in);
        String a = sa.next();

        System.out.println("ikinci sozu daxil edin:");
        Scanner sb = new Scanner(System.in);
        String b = sb.next();

        if(a.length() > b.length()) {
            System.out.println("bu string: " + a);
        }
        if(a.length() < b.length()) {
            System.out.println("bu string: " + b);
        } else {
            System.out.println("bu sozlerin string olcusu beraberdir");
        }
    }
}
