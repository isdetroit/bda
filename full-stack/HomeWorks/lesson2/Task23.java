package HomeWorks.lesson2;

import java.util.Scanner;  // Import the Scanner class
public class Task23 {
    public static void main(String args[]) {
        task23();
    }
    public static void task23() {
        int i = -5 + 8 * 6;
        int q = (55+9)%9;
        double a = 20.0 + (-3.0)*5.0/8.0;
        int s = 5 + 15 / 3 * 2 - 8 % 3;
        System.out.println("-5 + 8 * 6 = " + i);
        System.out.println("(55+9)%9= " + q);
        System.out.println("20 + (-3)*5/8= " +  a);
        System.out.println("5 + 15 / 3 * 2 - 8 % 3 = " + s);
    }
}