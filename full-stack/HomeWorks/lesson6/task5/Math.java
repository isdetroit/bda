package HomeWorks.lesson6.task5;

public class Math {
    int a;
    int b;

    public void toplama(int a , int b){
        this.a=a;
        this.b=b;
        System.out.println("toplama: " + (a+b));
    }

    public void cixma(int a, int b){
        this.a=a;
        this.b=b;

        System.out.println("cixma: " +(a-b));
    }

    public void faktorial(int a){
        this.a=a;
        int fak=1;
        for(int i=1;i<=a;i++){
            fak*=i;
        }

        System.out.println("faktorial: " + fak);
    }
}
