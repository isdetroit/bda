package HomeWorks.lesson7.task16_17_18;

public class Employee {
    public String name;
    public String departmentName;
    public String majority;
    public int salary;

    public Employee(String name, String departmentName, String majority, int salary) {
        this.name = name;
        this.departmentName = departmentName;
        this.majority = majority;
        this.salary = salary;

        System.out.println("name " + this.name);
        System.out.println("departmentName " + this.departmentName);
        System.out.println("majority " + this.majority);
        System.out.println("salary " + this.salary);
    }


    public void maasArtir( int b){
        this.salary+=b;
        System.out.println("yeni maas " + this.salary);
    }

    public void maasAzalt( int b){
        this.salary-=b;
        System.out.println("yeni maas " + this.salary);
    }

    public void vezifeDeyis(String c){
        if(c.length()==0){
            System.out.println("vezife yazmalisiniz");
        }else{
            this.majority=c;
            System.out.println("yeni vezife: " + this.majority);
        }
    }
}
