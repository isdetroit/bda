package HomeWorks.lesson7.task8;

public class Main {
    public static void main(String[] args) {
        Lamp lampa = new Lamp();

        lampa.veziyyetulLampa();
        lampa.sondur();
        lampa.yandir();
        lampa.yandir();
        lampa.sondur();

        lampa.veziyyetulLampa();
    }

}
