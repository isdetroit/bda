package HomeWorks.lesson13.task.services.impl;

import HomeWorks.lesson13.task.Exception.Messages;
import HomeWorks.lesson13.task.Exception.MyException;
import HomeWorks.lesson13.task.classes.User;
import HomeWorks.lesson13.task.services.interfaces.InterfaceOfServices;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Services implements InterfaceOfServices {
    ArrayList<User> users = new ArrayList<>();
    Scanner s = new Scanner(System.in);
    int currentYear=LocalDate.now().getYear();

    @Override
    public void showMenu() throws MyException {
        System.out.println("Linder Sosial Sebekesi -  " + currentYear );
        System.out.println(" 1 - register");
        System.out.println(" 2 - show users");
        System.out.println(" 3 - exit");

        System.out.println("Emrinizi daxil edin");
        String comm = s.next();
        switch (comm){
            case "1":
                register();
                break;
            case "2":
                showPeople();
                break;
            case "3":
                break;
        }
    }

    @Override
    public void register() throws MyException {
    try{
        System.out.println("username: ");
        String username = s.next();
        System.out.println("password: ");
        String password = s.next();
        if(password.length()<5){
            System.out.println(Messages.messageShortPassword);
            throw new MyException(Messages.messageShortPassword);
        }
        System.out.println("dogum iliniz:");
        int year = s.nextInt();
        int age = currentYear - year;
        if(age<18){
            System.out.println(Messages.messageNotA18);
            throw new MyException(Messages.messageNotA18);
        }
        User user = new User(username, year,password,age);
        users.add(user);
    }catch (Exception myException){
        System.out.println(Messages.messageWrongFormat);
        throw new MyException(Messages.messageWrongFormat);
    }
    finally{
        showMenu();
    }


    }

    @Override
    public void showPeople() throws MyException {
        System.out.println(users.toString());
        showMenu();
    }
}
