package HomeWorks.lesson13.task.Exception;

public class MyException extends Exception{

    public MyException(String message) {
        super(message);
    }
}
