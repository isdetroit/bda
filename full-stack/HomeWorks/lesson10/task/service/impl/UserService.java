package HomeWorks.lesson10.task.service.impl;

import HomeWorks.lesson10.task.model.Users;
import HomeWorks.lesson10.task.service.ServiceInterface;


import java.util.ArrayList;
import java.util.Scanner;

public class UserService implements ServiceInterface     {
    int indeks = 0;
    int tryChangePass = 0;
    ArrayList<Users> login = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);

    public void showMenu(){
        ArrayList<String> menu = new ArrayList<>();
        menu.add("Login - 1");
        menu.add("Register - 2");
        menu.add("Exit - 3");

        for (int i = 0; i < menu.size() ; i++) {
            System.out.println(menu.get(i));
        }


        System.out.println("Verdiyiniz emri reqemle bildirin:");
        String emrStr = scanner.next();
        switch (emrStr){
            case "1":
                login();
                break;
            case "2":
                register();
                break;
            case "3":
                break;

        }
    }

    public void register(){

        System.out.println("Qeydiyyat:");
        System.out.println("Login");
        String l = scanner.next();
        System.out.println("Parol:");
        String p = scanner.next();
        Users us = new Users(l,p);
        if(l.length()!=0 && p.length()!=0){
            login.add(us);
            System.out.println("Emeliyyat ugurla tamamlandi");
            showMenu();
        }else{
            System.out.println("ERROR!");
        }
    }
    public void showLoginMenu(){
        ArrayList<String> menu = new ArrayList<>();
        menu.add("Haqqında məlumatlar - 1 ");
        menu.add("Şifrəni dəyiş - 2");
        menu.add("Exit - 3");

        for (int i = 0; i < menu.size() ; i++) {
            System.out.println(menu.get(i));
        }
    }

    public void changePass() {

        System.out.println("kohne parol");
        String old = scanner.next();
        if(login.get(indeks).password.equals(old)){
            System.out.println("yeni parol");
            String new1 = scanner.next();
            System.out.println("yeni parolu tekrar edin");
            String new2 = scanner.next();
            if (new1.equals(new2)){
                login.get(indeks).password = new1;

                System.out.println("emeliyyat ugurla tamamlandi");
            }else{
                System.out.println("yeni parollar eyni deyil");
            }
        }else {
            System.out.println("kohne parol sehvdir");
            if(tryChangePass!=3){
                tryChangePass++;
                changePass();
            }else{
                System.out.println("kohne kodu 3 defe sehv yazdiniz");
            }
        }
    }

    public void login(){
        System.out.println("Giris");
        System.out.println("login");
        String newLog = scanner.next();
        System.out.println("parol");
        String newPass = scanner.next();

        boolean enter = false;


        for (int i = 0; i <login.size() ; i++) {
            if(login.get(i).username.equals(newLog) && login.get(i).password.equals(newPass)){
                enter = true;
                indeks=i;
            }
        }

        if (enter == true){
            System.out.println(login.get(indeks).username + ", xos gelmisiniz");
            showLoginMenu();
            System.out.println("Verdiyiniz emri reqemle bildirin:");
            String emrStr = scanner.next();
            switch (emrStr){
                case "1":
                    System.out.println(login.get(indeks).username + " userimizin adidir..");
                    break;
                case "2":
                    changePass();
                    break;
                case "3":
                    indeks=0;
                    showMenu();
                    break;

            }
        }else{
            System.out.println("ERROR LOGIN!");
        }
    }
}
