package HomeWorks.lesson10.task.service;

public interface ServiceInterface {
     void showMenu();
     void register();
     void showLoginMenu();
     void changePass();
     void login();
}
