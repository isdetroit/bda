package HomeWorks.lesson4;

import java.util.Scanner;

public class task10_11_12 {
    public static void main(String[] args) {
        System.out.println("10-cu task - iki eded daxil edin, bolme emeliyyati yerine yetirsin");
        System.out.println("a ededi:");
        Scanner sa = new Scanner(System.in);
        int a = sa.nextInt();

        System.out.println("b ededi:");
        Scanner sb = new Scanner(System.in);
        int b = sb.nextInt();

        System.out.println("a/b=" + a/b + "\n");


        System.out.println("11-cu task - misallari hell etsin" +
                "\n" +
                "a. -5 + 8 * 6\n" +
                "b. (55+9) % 9\n" +
                "c. 20 + -3*5 / 8\n" +
                "d. 5 + 15 / 3 * 2 - 8 % 3\n");

        System.out.println("a = " + (-5+8*6));
        System.out.println("b = " + ((55+9) % 9));
        System.out.println("c = " + (20 + -3*5 / 8));
        System.out.println("d = " + (5 + 15 / 3 * 2 - 8 % 3) + "\n");


        System.out.println("12-cu task - iki eded daxil edin, vurma emeliyyati yerine yetirsin");
        System.out.println("a ededi:");
        Scanner sq = new Scanner(System.in);
        int q = sq.nextInt();

        System.out.println("b ededi:");
        Scanner sw = new Scanner(System.in);
        int w = sw.nextInt();

        System.out.println("q*w=" + (q*w));

    }

}
