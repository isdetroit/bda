package WeekExam06_06_23.classes;

import WeekExam06_06_23.types.Gender;

public class Person {
    public  int id;
    public String name;
    public   String surname;
    public String fatherName;
    public int age;
    public Gender gender;

    public boolean isUser = false;


    public Person(int id, String name, String surname, String fatherName, int age, Gender gender) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.age = age;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", isUser=" + isUser +
                '}';
    }
}
