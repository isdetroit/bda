package WeekExam06_06_23.classes;

public class User {
    public int personId;
    public String username;
    public String password;

    public User(int personId, String username, String password) {
        this.personId = personId;
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "personId=" + personId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
