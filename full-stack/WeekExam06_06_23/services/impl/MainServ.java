package WeekExam06_06_23.services.impl;

import WeekExam06_06_23.classes.Person;
import WeekExam06_06_23.classes.User;
import WeekExam06_06_23.services.interface_service.InterfaceService;
import WeekExam06_06_23.types.Gender;

import java.util.ArrayList;
import java.util.Scanner;

public class MainServ implements InterfaceService {
    ArrayList<Person> persons = new ArrayList<>();
    ArrayList <User> users = new ArrayList<>();
    int personId = 0;
    Scanner scanner = new Scanner(System.in);


    public void showMenu(){
        System.out.println("1 - save person ");
        System.out.println("2 -  register user");
        System.out.println("3 - show person");
        System.out.println("4 - exit");

        System.out.println("emrinizi daxil edin.");
        String emr = scanner.next();

        switch (emr) {
            case "1" -> sp();
            case "2" -> ru();
            case "3" -> shp();
            case "4" -> exit();
        }





    }


    @Override
    public void sp() {
        System.out.println("ad:");
        String name = scanner.next();
        System.out.println("soyad:");
        String surname = scanner.next();
        System.out.println(" ata adi:");
        String fatherName = scanner.next();
        System.out.println("yash:");
        int age = scanner.nextInt();
        System.out.println("male/female");
        Gender gender = Gender.valueOf(scanner.next().toUpperCase());

        Person person = new Person(personId,name,surname,fatherName,age,gender);
        persons.add(person);
        personId+=1;
        showMenu();

    }

    @Override
    public void ru() {
        System.out.println("istifadecinin avtomatik verilmis id-sini qeyd edin ");
        int userId = scanner.nextInt();
        try{
            System.out.println(persons.get(userId).name + " adli istifadecinin user-ini yaradirsiniz");
            System.out.println("username:");
            String username = scanner.next();
            System.out.println("password");
            String password = scanner.next();
            User user = new User(userId,username,password);
            users.add(user);
            persons.get(userId).isUser =true;
        }catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println("yazdiginiz id uygun deyil");
        }
        finally{
            showMenu();
        }
    }

    @Override
    public void shp() {
        System.out.println(persons.toString());
        showMenu();
    }

    @Override
    public void exit() {
        System.out.println("cixis edildi");
    }
}
