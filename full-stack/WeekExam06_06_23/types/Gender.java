package WeekExam06_06_23.types;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    String gender;

    Gender(String gender) {
        this.gender = gender;
    }
}
