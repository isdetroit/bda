package ProjectInClass.services.interfaces;

import HomeWorks.lesson13.task.Exception.MyException;

public interface First {
    void register() throws MyException;

    void startMatch() throws MyException;

    void guessWinner() throws MyException;

    void enterUser() throws MyException;

    void logoutUser() throws MyException;

    void exitAdmin();
}
