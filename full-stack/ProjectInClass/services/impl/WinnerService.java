package ProjectInClass.services.impl;

import HomeWorks.lesson13.task.Exception.MyException;
import ProjectInClass.Exception.Messages;
import ProjectInClass.classes.Competitor;
import ProjectInClass.services.interfaces.First;

import java.util.ArrayList;
import java.util.Scanner;

public class WinnerService  implements First {
    ArrayList<Competitor> competitors = new ArrayList<>();
    int winnerIndex = -1;

    int enterIndex = -1;


    int countComp = 0;
    Scanner scanner = new Scanner(System.in);


    public void showAdminMenu() throws MyException {
        System.out.println("Admin Menu");
        System.out.println("1- Yarismacilari register et");
        System.out.println("2- Yarisma baslat");
        System.out.println("3- udani texmin et");
        System.out.println("4- enter User ");
        System.out.println("5- exit");

        System.out.println("Emrinizi daxil edin:");

        String emr = scanner.next();

        switch (emr) {
            case "1" -> register();
            case "2" -> startMatch();
            case "3" -> guessWinner();
            case "4" -> enterUser();
            case "5" -> exitAdmin();
        }
    }

    public void showUserMenu() throws MyException {
        System.out.println("User Menu");
        System.out.println("1- Logout");
        System.out.println("2- About Me");

        System.out.println("Emrinizi daxil edin:");

        String emr = scanner.next();

        switch (emr) {
            case "1" -> logoutUser();
            case "2" -> aboutMe();
        }
    }

    public void aboutMe() throws MyException {
        System.out.println(competitors.get(enterIndex).toString());
        showUserMenu();
    }

    @Override
    public void register() throws MyException {
        System.out.println("Istirakci sayi:");
        countComp = scanner.nextInt();


        boolean isWinner = false;

        for (int i = 0; i < countComp; i++) {
            System.out.println("username");
            String name = scanner.next();
            System.out.println("password");
            String password = scanner.next();
            boolean isLowerCaseRuleBroke = false;

            for (int j = 0; j <name.length() ; j++) {
                if(!Character.isLowerCase(name.charAt(i))){
                    isLowerCaseRuleBroke = true;
                }
            }
            if(!isLowerCaseRuleBroke){
                if(password.length()>=5){
                    Competitor competitor = new Competitor(name, password, isWinner);
                    competitors.add(competitor);
                    System.out.println((i + 1) + ". istirakci daxil olundu ");
                }else{
                    System.out.println(Messages.lengthPasswordRule);
                    throw new MyException(Messages.lengthPasswordRule);
                }
            }else{
                System.out.println(Messages.lowCaseRule);
                throw new MyException(Messages.lowCaseRule);

            }


        }

        showAdminMenu();
    }

    @Override
    public void startMatch() throws MyException {
        int max = countComp;
        int min = 1;
        int range = max - min + 1;

        winnerIndex = (int) (Math.random() * range) + min - 1;

        competitors.get(winnerIndex).isWinner = true;

        System.out.println("test winnner: " + competitors.get(winnerIndex).toString());
        showAdminMenu();

    }

    @Override
    public void guessWinner() throws MyException {
        System.out.println("Sence necenci istirakci udub udub?");
        int guess = scanner.nextInt();
        if (guess == winnerIndex + 1) {
            System.out.println("dogru texmindir, udan: " + competitors.get(winnerIndex).toString());
        } else {
            System.out.println("yanlis texmin");
        }
        showAdminMenu();

    }

    @Override
    public void enterUser() throws MyException {
        System.out.println("username");
        String username = scanner.next();
        System.out.println("password");
        String password = scanner.next();
        for (int i = 0; i < countComp; i++) {
            System.out.println(i+ " i");
            if (competitors.get(i).username.equals(username) && competitors.get(i).password.equals(password)) {
                enterIndex = i;
                System.out.println(username + ", xos gelmisiniz");
                showUserMenu();
            }

        }

        if(enterIndex==-1){
            System.out.println("Xeta..");
            showAdminMenu();
        }
    }

    @Override
    public void logoutUser() throws MyException {
        System.out.println("User-den cixis edildi");
        enterIndex = -1;
        showAdminMenu();
    }

    @Override
    public void exitAdmin() {
        System.out.println("Admin-den cixis edildi");
        winnerIndex = -1;

    }
}
