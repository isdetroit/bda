package ProjectInClass;

import HomeWorks.lesson13.task.Exception.MyException;
import ProjectInClass.services.impl.WinnerService;

public class Main {
    public static void main(String[] args) throws MyException {
        WinnerService winner = new WinnerService();
        winner.showAdminMenu();
    }
}
