package ProjectInClass.classes;

public class Competitor {
    public String username;
    public String password;
    public boolean isWinner;

    public Competitor(String username, String password, boolean isWinner) {
        this.username = username;
        this.password = password;
        this.isWinner = isWinner;
    }

    @Override
    public String toString() {
        return "Competitor{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isWinner=" + isWinner +
                '}';
    }
}
