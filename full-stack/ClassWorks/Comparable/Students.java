package ClassWorks.Comparable;

public class Students implements Comparable<Students> {
    public String name;
    public Integer age;

    public Students(String name, Integer age){
        this.name = name;
        this.age = age;
    }


    @Override
    public int compareTo(Students o) {
        int k = name.compareTo(o.name);
        if(k!=0){
            System.out.println(k);
            return k;
        }
        return age.compareTo(o.age);
    }
}
