package ClassWorks.Comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Students a = new Students("wjsahdwjh", 978);
        System.out.println(a.age);
        Students b = new Students("wadjn", 99);
        System.out.println(b.age);




        ArrayList<Students> al = new ArrayList<>();
        al.add(new Students("aaa",12));
        al.add(new Students("eeeee",12));
        al.add(new Students("aaa",76));
        al.add(new Students("ccccc",9));
        al.add(new Students("aaa",11));

        ArrayList<String> al2 = new ArrayList<>();
        al2.add("bbbbb");
        al2.add("aaaaa");
        al2.add("ccccc");
        al2.add("mmmmm");

        Collections.sort(al);

        for (Students s: al) {
            System.out.println(s.name + " " + s.age);
        }
    }
}
