package ClassWorks.OOP_Constructor;

public class Book {
    private String author;
    private int page;
    private int weight;

    public Book(String author, int page, int weight) {
        this.author = author;
        this.page = page;
        this.weight = weight;
    }



    public static void main(String[] args) {
        Book b = new Book("Mikayil Musfiq", 7,2);
        System.out.println(b.author + " " + b.page + " " + b.weight);

    }
}
