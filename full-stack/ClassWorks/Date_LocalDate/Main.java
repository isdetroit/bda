package ClassWorks.Date_LocalDate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2020, Month.JANUARY,20);
        LocalTime time = LocalTime.of(11,12,32);
        LocalDateTime dateTime = LocalDateTime.of(date,time);

        DateTimeFormatter mediumDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
        DateTimeFormatter longDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        DateTimeFormatter shortDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        DateTimeFormatter mediumDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
//        DateTimeFormatter longDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL);

        System.out.println(mediumDate.format(date));
        System.out.println(longDate.format(date));
        System.out.println(shortDate.format(date));
        System.out.println(mediumDateTime.format(dateTime));
        System.out.println(shortDateTime.format(dateTime));
//        System.out.println(longDateTime.format(dateTime));

    }
}
