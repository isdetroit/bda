package ClassWorks.lesson12;

public enum WeekOfDays {
    MONDAY("Bazar ertesi"),
    TUESDAY("Çərşənbə axşamı"),
    WEDNESDAY("Çərşənbə"),
    THURSDAY("Cümə axşamı"),
    FRIDAY("Cümə"),
    SATURDAY("Şənbə"),
    SUNDAY("Bazar");

    private String aze;

    WeekOfDays(String  aze) {
        this.aze=aze;
    }

    public String getAze() {
        return aze;
    }
}
