package ClassWorks.Generics.classes;

import ClassWorks.Generics.types.GenericClass;

public class GenericMethods {

    public static  <T> boolean isEqual(GenericClass a, GenericClass b){
        System.out.println(a.getData().equals(b.getData()));
        return a.getData().equals(b.getData());
    }
}
