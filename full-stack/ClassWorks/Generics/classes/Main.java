package ClassWorks.Generics.classes;

import ClassWorks.Generics.types.GenericClass;


public class Main {
    public static void main(String[] args) {
        GenericClass<Object> a = new GenericClass("jjhegdhg");
        System.out.println( a.getData() );

        GenericMethods e = new GenericMethods();

        GenericClass aa = new GenericClass<>();
        GenericClass bb = new GenericClass<>();

        aa.setData(4);
        bb.setData("wsd");

        e.isEqual(aa,bb);
    }


}
