package ClassWorks.Generics.types;

public class GenericClass<T> {

    private T data ;

    public GenericClass(T data) {
        this.data = data;
    }

    public GenericClass() {
    }

    public T getData() {
        System.out.println("bizim getirdiyimiz datanin tipi : " + data.getClass().getSimpleName());
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
